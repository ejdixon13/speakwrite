import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AlertController, FabContainer, NavController, PopoverController } from 'ionic-angular';
import { filter, get, last } from 'lodash';
import { Subscription } from "rxjs/Subscription";
import { DocumentModel } from "../../models/documents.model";
import { FolderModel } from "../../models/folders.model";
import { DocumentService } from "../../services/document.service";
import { FolderService } from "../../services/folder.service";
import { EditorPage } from "../editor/editor";
import { CardPopover } from "../popovers/card-popover";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  documents: Array<DocumentModel>;
  folderStack : Array<FolderModel> = [];
  refreshDocsSubscription: Subscription;
  showItems: boolean = false;
  loadingDocuments: boolean = false;
  
  quillForm = this.formBuilder.group({
    quillEditorDelta: null
  })

  constructor(@Inject(NavController) public navCtrl: NavController,
    private docService: DocumentService,
    private folderService: FolderService,
    private popoverCtrl: PopoverController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,

  ) {

    this.refreshDocsSubscription = docService.docsRefreshed$.subscribe(
      (documents:DocumentModel[]) => {
        this.documents = filter(documents, {folderId: this.currentFolder.id});
        this.showItems = true;
        this.loadingDocuments = false;
      });

  }

  get currentFolder(){
    return last(this.folderStack);
  }

  pushFolder(folder:FolderModel) {
    this.showItems = false;
    this.loadingDocuments = true;
    this.folderStack.push(folder);
    this.docService.refreshDocs();
  }

  popFolder() {
    if(this.folderStack.length > 1) {
      this.showItems = false;
      this.folderStack.pop();
      this.docService.refreshDocs();
    }
  }

  createNewDoc(event, fab: FabContainer) {
    fab.close();
    let alert = this.alertCtrl.create({
      title: 'New Document',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        }
      ],
      buttons: [
        {
          text: 'Create',
          handler: data => {
            //TODO: constrict title length
            // if(data.title.length > 30) {
            //   alert('Title too long');
            // }
            if(data.title){
              this.docService.createNewDocument(data.title, this.currentFolder.id)
                .then((doc:DocumentModel)=>{
                  this.docService.refreshDocs();
                  this.pushPage(doc.id);
                })
            } else {
              return;
            }
          }
        }
      ]
    });
    alert.present();

  }

  createNewFolder(event, fab:FabContainer) {
    fab.close();
    let alert = this.alertCtrl.create({
      title: 'New Folder',
      inputs: [
        {
          name: 'title',
          placeholder: 'Folder name'
        }
      ],
      buttons: [
        {
          text: 'Create',
          handler: data => {
            if(data.title){
              this.folderService.createNewFolder(data.title, this.currentFolder.id)
                .then((newFolder:FolderModel)=> this.currentFolder.folders.push(newFolder))
            } else {
              return;
            }
          }
        }
      ]
    });
    alert.present();
  }

  pushPage(id:any){
    // push another page onto the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    this.navCtrl.push(EditorPage, {
      id: id
    });
  }

  presentItemPopover(myEvent:Event, itemType, id, parentFolderId?:number) {
    myEvent.stopPropagation();

    let popover = this.popoverCtrl.create(CardPopover, {id: id, parentFolderId: parentFolderId, type: itemType});

    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((data) => {
      if(get(data, 'folderDeleted')) {
        this.folderService.getRootFolder().then((rootFolder)=>{
          let newFolderStack = [];
          while(this.folderStack.length > 0) {
            let folderToReplace  = this.folderStack.pop();
            let folderReplacement = this.folderService.getFolder(rootFolder, folderToReplace.id);
            newFolderStack.push(folderReplacement);
          }


          this.folderStack = newFolderStack.reverse(); // reverse because they were inserted backwards
        })
      }
    })
  }

  ngOnInit() {
    this.folderService.getRootFolder().then((folder)=>{
      this.folderStack.push(folder);
      this.docService.refreshDocs();
    })
  }

  ngOnDestroy() {
    this.refreshDocsSubscription.unsubscribe();
    // this.refreshFoldersSubscription.unsubscribe();
  }

}
