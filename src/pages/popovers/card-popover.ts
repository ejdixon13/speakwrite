import { Component } from "@angular/core";
import { NavParams, ViewController, AlertController } from "ionic-angular";
import { get } from 'lodash';
import { DocumentService } from "../../services/document.service";
import { FolderService } from "../../services/folder.service";

@Component({
  template: `
    <ion-list>
      <ion-list-header>Actions</ion-list-header>
      <button ion-item (click)="deleteAction()">Delete</button>
    </ion-list>
  `
})
export class CardPopover {
  constructor(public viewCtrl: ViewController,
              public navParams:NavParams,
              public folderService: FolderService,
              private alertCtrl: AlertController,
              public docService: DocumentService) {}

  deleteAction() {
    // DELETE DOC
    if (this.navParams.data.type == 'doc') {
      this.showDeleteConfirmation(
        () => this.docService
          .deleteDocument(this.navParams.data.id)
          .then(() => {
            this.docService.refreshDocs();
            this.viewCtrl.dismiss({});
          })
      )
      
    }
    // DELETE FOLDER
    else if (this.navParams.data.type == 'folder' && get(this.navParams, 'data.id', false) && get(this.navParams, 'data.parentFolderId', false)) {
      this.showDeleteConfirmation(
        () => this.folderService
          .deleteFolder(this.navParams.data.id, this.navParams.data.parentFolderId)
          .then(() => {
            this.docService.refreshDocs();
            this.viewCtrl.dismiss({ folderDeleted: true });
          })
      )
    }
  }

  showDeleteConfirmation(onConfirmAction: () => void) {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure you want to delete this item?',
      message: '(This item cannot be recovered)',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            onConfirmAction();
          }
        }
      ]
    });
    confirm.present();
  }
}
