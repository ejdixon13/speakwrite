import {Component, Inject} from '@angular/core';
import {NavController} from "ionic-angular";
import {UserService} from "../../services/user.service";


@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {

  constructor(@Inject(NavController) public navCtrl: NavController, private userService:UserService) {}

  completeTutorial() {
    this.userService.setAppBeenOpenedPreviously(true)
      .then(()=> this.navCtrl.pop());
  }


  ngOnInit() {
  }


}
