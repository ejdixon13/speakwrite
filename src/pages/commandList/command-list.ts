import { Component, Inject } from '@angular/core';
import { NavController } from "ionic-angular";
import { PLAYBACK, PLAY_LAST_SENTENCE, PLAY_LAST_PARAGRAPH, PLAY_LAST_WORD, PLAY_ALL, SCRATCH_THAT, DELETE_LAST_SENTENCE, ERASE_LAST_SENTENCE, DELETE_LAST_WORD, ERASE_LAST_WORD, NAVIGATE_ONE_WORD_BACK, NAVIGATE_ONE_WORD_FORWARD, GO_TO_END, GO_TO_THE_END, OPEN_QUOTE, CLOSE_QUOTE, SEMICOLON, COLON, CAPITALIZE, UNCAPITALIZE } from '../../services/command.service';
import { Device } from '@ionic-native/device';
import { IS_FULLY_SUPPORTED_ANDROID_VERSION } from '../../models/app.const';

const COMMAND_LIST = [
  {
    category: "Playback",
    color: "blue",
    commands: [
      {
        name: PLAYBACK,
        description: "Reads back sentence before the cursor or that the cursor intersects",
        alternatePhrases: [PLAY_LAST_SENTENCE]
      },
      {
        name: PLAY_LAST_PARAGRAPH,
        description: "Reads back paragraph before the cursor or that the cursor intersects",
        alternatePhrases: []
      },
      {
        name: PLAY_LAST_WORD,
        description: "Reads back last word",
        alternatePhrases: []
      },
      {
        name: PLAY_ALL,
        description: "Reads back all text in document",
        alternatePhrases: []
      }
    ]
  },
  {
    category: "Edit",
    color: "orange",
    commands: [
      {
        name: SCRATCH_THAT,
        description: "Removes last captured phrase",
        alternatePhrases: []
      },
      {
        name: DELETE_LAST_SENTENCE,
        description: "Deletes last sentence before the cursor or that the cursor intersects",
        alternatePhrases: [ERASE_LAST_SENTENCE]
      },
      {
        name: DELETE_LAST_WORD,
        description: "Deletes highlighted word",
        alternatePhrases: [ERASE_LAST_WORD]
      },
      {
        name: OPEN_QUOTE,
        description: "Inserts '\"' with no space after",
        alternatePhrases: []
      },
      {
        name: CLOSE_QUOTE,
        description: "Inserts '\"' with no space before or after",
        alternatePhrases: []
      },
      {
        name: CAPITALIZE,
        description: "Capitalizes first letter of current word",
        alternatePhrases: []
      },
      {
        name: UNCAPITALIZE,
        description: "Uncapitalizes first letter of current word",
        alternatePhrases: []
      }
    ]
  },
  {
    category: "Navigation",
    color: "green",
    commands: [
      {
        name: NAVIGATE_ONE_WORD_BACK,
        description: "Navigates one word backward",
        alternatePhrases: []
      },
      {
        name: "BACK X WORDS (X IS 2, 3, OR 4)",
        description: "Navigates x words backward",
        alternatePhrases: []
      },
      {
        name: NAVIGATE_ONE_WORD_FORWARD,
        description: "Navigates one word forward",
        alternatePhrases: []
      },
      {
        name: "NEXT X WORDS (X IS 2, 3, OR 4)",
        description: "Navigates x words forward",
        alternatePhrases: []
      },
      {
        name: GO_TO_END,
        description: "Navigates to the end of the document",
        alternatePhrases: [GO_TO_THE_END]
      },
    ]
  }
];

@Component({
  selector: 'page-command-list',
  templateUrl: 'command-list.html'
})
export class CommandListPage {

  commandList = COMMAND_LIST;

  constructor(@Inject(NavController) public navCtrl: NavController, private device: Device) {}


  get isFullySupportedAndroidVersion() {
    return IS_FULLY_SUPPORTED_ANDROID_VERSION(this.device.version);
  }

  get filteredCommands() {
    return COMMAND_LIST.filter(command => {
      return IS_FULLY_SUPPORTED_ANDROID_VERSION(this.device.version) || (command.category !== 'Navigation');
    })
  }

  ngOnInit() {
  }


}
