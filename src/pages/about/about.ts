import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  searchTerm: FormControl = new FormControl();

  constructor(public navCtrl: NavController) {

  }


  ngOnInit() {
  }

}
