import {Component, Inject, OnDestroy} from '@angular/core';
import {NavController} from "ionic-angular";
import { SettingsService, Settings } from '../../services/settings.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { TextToSpeech } from '../../custom-external/text-to-speech';
import { LOCALE } from '../../models/app.const';
import { Subscription } from 'rxjs';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage implements OnDestroy {

  constructor(
    @Inject(NavController) public navCtrl: NavController,
    private settingsService: SettingsService,
    private formBuilder: FormBuilder,
    private tts: TextToSpeech
  ) {
    this.form = this.formBuilder.group({
      playbackSpeed: null,
      playbackImmediately: false,
      fontSizeWhileRecording: null
    })
  }


  form: FormGroup;
  settingsSub: Subscription;
  formChangeSub: Subscription;
  playbackSpeedSub: Subscription;

  ngOnInit() {
    this.setupForm();
  }

  private setupForm() {
    this.settingsSub = this.settingsService
      .state$
      .subscribe(settings => {
        this.form.patchValue(settings, { emitEvent: false });
      });

    this.formChangeSub = this.form
      .valueChanges
      .pipe(
        debounceTime(500)
      )
      .subscribe((value) => {
        this.saveSettings();
      })
    
    this.playbackSpeedSub = this.form.get('playbackSpeed')
    .valueChanges
    .pipe(
      debounceTime(500)
    ).subscribe(() => {
      this.tts
        .speak({ text: "The Quick Brown Fox", locale: LOCALE, rate: this.playbackSpeed, pitch: 1 })
    })
  }

  get playbackSpeed() {
    return (new Settings(this.form.value)).TTSPlaybackSpeed;
  }

  get fontSizeClass() {
    return  'ws-settings__font-size-' + this.form.get('fontSizeWhileRecording').value;
  }

  saveSettings() {
    this.settingsService.saveSettings(this.form.value);
  }

  ngOnDestroy() {
    this.settingsSub.unsubscribe();
    this.formChangeSub.unsubscribe();
    this.playbackSpeedSub.unsubscribe();
  }


}
