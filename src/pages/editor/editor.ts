import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Clipboard } from '@ionic-native/clipboard';
import { Insomnia } from "@ionic-native/insomnia";
import { SpeechRecognition } from "@ionic-native/speech-recognition";
// import { AdMob } from 'ionic-admob';
import { NavParams, Platform, ToastController } from "ionic-angular";
import { of } from "rxjs/observable/of";
import { QuillEditorService } from "../../components/quill-editor/quill-editor.service";
import { DocumentModel } from "../../models/documents.model";
import { Paragraph, Sentence, WordPunct } from "../../models/grammar.model";
import { CommandService, DELETE_LAST_SENTENCE, DELETE_LAST_WORD, GO_TO_END, NAVIGATE_ONE_WORD_BACK, NAVIGATE_TWO_WORDS_BACK, NAVIGATE_TWO_WORDS_FORWARD, NAVIGATE_ONE_WORD_FORWARD } from "../../services/command.service";
import { DocumentService } from "../../services/document.service";
import { AdService } from '../../services/ad.service';
import { SettingsService } from '../../services/settings.service';


@Component({
  selector: "page-contact",
  templateUrl: "editor.html"
})
export class EditorPage {
  TAG: string = "JARVIS APP";

  microphoneOn: Boolean = false;
  microphoneOnFontSize: number;
  volume: number = 1;
  documentModel: DocumentModel;
  public editor;
  public editorOptions = {
    placeholder: "Once upon a time..."
  };
  testWord: WordPunct;
  testSentence: Sentence;
  testParagraph: Paragraph;
  showDebug: boolean = false;
  

  quillForm = this.formBuilder.group({
    quillEditorDelta: null
  });

  command: string = '';
  commands: string[] = [
    DELETE_LAST_SENTENCE,
    DELETE_LAST_WORD,
    NAVIGATE_ONE_WORD_BACK,
    NAVIGATE_TWO_WORDS_BACK,
    NAVIGATE_TWO_WORDS_BACK,
    NAVIGATE_ONE_WORD_FORWARD,
    NAVIGATE_TWO_WORDS_FORWARD,
    GO_TO_END,
    'HERE ARE SOME WORDS'
  ]


  constructor(
    private speechRecognition: SpeechRecognition,
    private commandService: CommandService,
    platform: Platform,
    private insomnia: Insomnia,
    private navParams: NavParams,
    private docService: DocumentService,
    private clipboard: Clipboard,
    private quillEditorService: QuillEditorService,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private adService: AdService,
    private settingService: SettingsService
    
  ) {
    platform.ready().then(() => this.adService.prepareBannerAd() )
  }

  copyToClipboard() {
    this.clipboard.copy(this.quillEditorService.getCurrentEditor().getText())
    let toast = this.toastCtrl.create({
      message: 'All Text Copied!',
      duration: 3000
    })
    toast.present();
  }

  speakTestPhrase() {
    this.commandService.speak("Test Phrase");
  }

  startListening() {
    this.microphoneOn = true;
    this.commandService.startListening();
    this.quillEditorService.getCurrentEditor().disable();
    this.insomnia.keepAwake();
    setTimeout(() => this.quillEditorService.scrollToCurrentWord(), 100);
  }

  stopListening() {
    this.microphoneOn = false;
    this.commandService.stopListening();
    this.quillEditorService.getCurrentEditor().enable();
    this.insomnia.allowSleepAgain();
  }

  parseCommand(command: string) {
    this.commandService.parseCommand([command]);
  }

  get currentSelection() {
    return this.quillEditorService.getCurrentEditor() ? this.quillEditorService.getCurrentEditor().getSelection() : 'Loading...';
  }

  getSentenceBeforeCursor() {
    this.testSentence = this.quillEditorService.getSentenceAtPosition();
  }

  getParagraphBeforeCursor() {
    this.testParagraph = this.quillEditorService.getParagraphBeforePosition();
  }

  deleteSentenceBeforeCursor() {
    this.quillEditorService.deleteSentenceBeforePosition();
  }

  setVolume() {
    this.commandService.setPlaybackVolume(this.volume);
  }

  get recordingFontSize() {
    let fontSizeClass = 'editor-page__editor--font-size-';
    if (this.microphoneOn) {
      fontSizeClass += this.microphoneOnFontSize;
    } else {
      fontSizeClass + '1';
    }
    return fontSizeClass;
  }

  ngOnInit() {
    let documentId = this.navParams.get("id");

    if (documentId) {
      this.docService.getDocument(this.navParams.get("id")).then((documentModel: DocumentModel) => {
        this.quillForm.patchValue({
          quillEditorDelta: documentModel.doc
        });
        this.documentModel = documentModel;
      });

      this.quillForm
        .get("quillEditorDelta")
        .valueChanges
        .debounceTime(1000)
        .switchMap(({ editor, html, text }) =>
          editor ? this.docService.updateDocument(documentId, editor.getContents(), html) : of([])
        )
        .subscribe(() => console.log("Document Updated!"));
    } else {
      console.error("Requires doc Id!");
    }

    this.quillForm
      .get("quillEditorDelta")
      .valueChanges
      .switchMap(value => of(value.text))
      .distinctUntilChanged()
      .debounceTime(100)
      .subscribe(() => this.quillEditorService.updateHighlightedWord());

    setTimeout(() => {
      this.quillEditorService.setCursorToEnd();
      let element = document.querySelector('span[style*="background-color: pink;"]');
      // element.scrollIntoView();
    }, 500);
    this.setupSpeechRecognition();

    if (this.microphoneOn) {
      this.insomnia.keepAwake();
    }

    this.settingService.state$.subscribe(settings => {
      this.microphoneOnFontSize = settings.fontSizeWhileRecording;
    })

    
  }

  onEditorTouch() {
    this.quillEditorService.getCurrentEditor().enable();
    this.quillEditorService.updateHighlightedWord();
    this.quillEditorService.scrollToCurrentWord();
  }

  setupSpeechRecognition() {
    // Check feature available
    this.speechRecognition.isRecognitionAvailable().then((available: boolean) => console.log(available));

    // Get the list of supported languages
    this.speechRecognition
      .getSupportedLanguages()
      .then((languages: Array<string>) => console.log(languages), error => console.log(error));

    // Check permission
    this.speechRecognition.hasPermission().then((hasPermission: boolean) => console.log(hasPermission));

    // Request permissions
    this.speechRecognition.requestPermission().then(() => console.log("Granted"), () => console.log("Denied"));
  }


  ionViewDidLeave() {
    this.docService.refreshDocs();
    this.adService.hideBannerAd();
    this.insomnia.allowSleepAgain();
  }

  toast(phrase:string) {
    let toast = this.toastCtrl.create({
      message: phrase,
      duration: 3000
    })
    toast.present();
  }

}
