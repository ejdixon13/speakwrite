import {Injectable} from '@angular/core';
import {DocumentModel} from "../../models/documents.model";
import {DocumentService} from "../../services/document.service";
import {WordsService} from "../../services/words.service";
import {RichTextElementService} from "../../components/rich-text/rich-text-element.service";


@Injectable()
export class EditorService {

  TAG : string = 'JARVIS APP';


  constructor(private documentService: DocumentService, private wordsService: WordsService, private richTextElementService: RichTextElementService) {}

  //Sets up Word service and Rich text element
  setupDocument(documentId: any) {
    // this.richTextElementService.bindDocument(documentId);
    // return this.documentService.getDocument(documentId)
    //   .then((doc:DocumentModel) =>{
    //     this.wordsService.setWords(doc.body);
    //     return doc.title;
    //   });
  }
}
