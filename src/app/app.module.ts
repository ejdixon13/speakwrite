import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule, JsonpModule } from "@angular/http";
import { BrowserModule } from '@angular/platform-browser';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { Clipboard } from '@ionic-native/clipboard';
import { Insomnia } from "@ionic-native/insomnia";
import { Keyboard } from "@ionic-native/keyboard";
import { SpeechRecognition } from "@ionic-native/speech-recognition";
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from "@ionic/storage";
import { FabContainer, IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { QuillEditorService } from "../components/quill-editor/quill-editor.service";
import { QuillEditorModule } from '../components/quill-editor/quillEditor.module';
import { RichTextElementService } from "../components/rich-text/rich-text-element.service";
import { RichTextComponentModule } from "../components/rich-text/rich-text.module";
import { TextToSpeech } from "../custom-external/text-to-speech/index";
import { AboutPage } from '../pages/about/about';
import { CommandListPage } from "../pages/commandList/command-list";
import { EditorPage } from '../pages/editor/editor';
import { EditorService } from "../pages/editor/editor.service";
import { HomePage } from '../pages/home/home';
import { CardPopover } from "../pages/popovers/card-popover";
import { SettingsPage } from "../pages/settings/settings";
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from "../pages/tutorial/tutorial";
import { CommandService } from "../services/command.service";
import { DocumentService } from "../services/document.service";
import { FolderService } from "../services/folder.service";
import { JarvisService } from "../services/jarvis.service";
import { RecordingService } from "../services/recording.service";
import { UserService } from "../services/user.service";
import { WordsService } from "../services/words.service";
import { MyApp } from './app.component';
import { SettingsService } from '../services/settings.service';
import { AdMobPro } from '@ionic-native/admob-pro';
import { AdService } from '../services/ad.service';
import { PaidVsFreeService } from '../services/paidVsFree.service';
import { Device } from '@ionic-native/device';



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    EditorPage,
    SettingsPage,
    CommandListPage,
    HomePage,
    TabsPage,
    CardPopover,
    TutorialPage
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RichTextComponentModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    QuillEditorModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    EditorPage,
    HomePage,
    TutorialPage,
    SettingsPage,
    CommandListPage,
    TabsPage,
    CardPopover
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SpeechRecognition,
    TextToSpeech,
    RecordingService,
    CommandService,
    JarvisService,
    WordsService,
    RichTextElementService,
    DocumentService,
    FolderService,
    EditorService,
    FabContainer,
    Insomnia,
    Keyboard,
    UserService,
    QuillEditorService,
    AndroidPermissions,
    BluetoothSerial,
    SettingsService,
    PaidVsFreeService,
    AdService,
    Clipboard,
    AdMobPro,
    Device,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
