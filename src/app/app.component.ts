import { Component, ViewChild } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MenuController, Platform } from 'ionic-angular';
import { timer } from 'rxjs/observable/timer';
import { AboutPage } from "../pages/about/about";
import { CommandListPage } from "../pages/commandList/command-list";
import { HomePage } from '../pages/home/home';
import { SettingsPage } from "../pages/settings/settings";
import { TutorialPage } from "../pages/tutorial/tutorial";
import { UserService } from "../services/user.service";
import { AdMobPro } from '@ionic-native/admob-pro';
import { AdService } from '../services/ad.service';
import { PaidVsFreeService } from '../services/paidVsFree.service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('ionNavRoot') nav
  rootPage:any = HomePage;
  tutorialPage:any = TutorialPage;
  settingsPage:any = SettingsPage;
  aboutPage: any = AboutPage;
  commandListPage:any = CommandListPage;
  homePage: any = HomePage;
  showSplash = true;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    userService: UserService,
    private menuCtrl: MenuController,
    private androidPermissions: AndroidPermissions,
    private paidVsFreeService: PaidVsFreeService
  ) {
    platform.ready().then(() => {
      userService.setAppBeenOpenedPreviously(false); // TODO: remove when deploying to app store
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      userService.hasAppBeenOpenedPreviously()
        .then((hasAppBeenOpenedPreviously: boolean) => {
          if (!hasAppBeenOpenedPreviously) {
            this.nav.push(TutorialPage);
            userService.setAppBeenOpenedPreviously(true);
            this.paidVsFreeService.setDefaultWordLimit();
          }
          statusBar.styleDefault();
          splashScreen.hide();

          timer(1000).subscribe(() => this.showSplash = false)
        })

    });
  }

  openPage(page:any) {
    this.menuCtrl.close();
    this.nav.push(page);
  }
}
