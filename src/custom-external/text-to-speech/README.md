<a style="float:right;font-size:12px;" href="http://github.com/ionic-team/ionic-native/edit/master/src/@ionic-native/plugins/text-to-speech/index.ts#L10">
  Improve this doc
</a>

# Text To Speech

```
$ ionic cordova plugin add cordova-plugin-tts
$ npm install --save @ionic-native/text-to-speech
```

## [Usage Documentation](https://ionicframework.com/docs/native/text-to-speech/)

Plugin Repo: [https://github.com/vilic/cordova-plugin-tts](https://github.com/vilic/cordova-plugin-tts)

Text to Speech plugin

## Supported platforms
- Android
- iOS
- Windows Phone 8



