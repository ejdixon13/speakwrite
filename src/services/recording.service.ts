import { ApplicationRef, Injectable } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { TextToSpeech } from '../custom-external/text-to-speech';


@Injectable()
export class RecordingService {

  TAG: string = 'JARVIS APP';


  constructor(private ref: ApplicationRef, private tts: TextToSpeech, private androidPermissions: AndroidPermissions, private bluetoothSerial: BluetoothSerial) {}


  startListening(callback): void {
    // this.bluetoothSerial.isEnabled().then(value => {
    //   console.log(value);
    //   this.checkBluetoothPermissions()
    // });
    (<any>window).ContinuousVoiceRecognizer.startRecognize((matches) => {
        this.ref.tick();
        callback(matches);
        console.log('Voice Recognition Success:' + matches);
      }, function(err) {
        console.log('Voice Recognition Error: ' + err, 'background: #222; color: #bada55');
      }, 1000);
  }

  private checkBluetoothPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH).then(
      result => {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.BLUETOOTH])
          .then(value => {
            console.log(value);
          });
        console.log('Has permission?', result.hasPermission)
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH)
    );

    //TODO: move to shared method
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(
      result => {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS])
          .then(value => {
            console.log(value);
          });
        console.log('Has permission?', result.hasPermission)
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS)
    );

  }

  // startListening() {
  //   (<any>window).ContinuousVoiceRecognizer.startRecognize((matches) => {
  //     console.log(this.TAG + matches);
  //     this.parseCommand(matches);
  //
  //     this.allMatches = matches;
  //     this.ref.tick();
  //   }, function(err) {
  //     console.log('Error: ' + err);
  //   }, 1000);
  // }

  stopListening(callback): void {
      (<any>window).ContinuousVoiceRecognizer.stopRecognize(() => {
        this.ref.tick();
        callback(null);
      }, function(err) {
        console.log('Error: ' + err);
      }, 1000);
  }

  setPlaybackVolume(volume, callback): void {
    (<any>window).ContinuousVoiceRecognizer.setPlaybackVolume(() => {
      console.log('after playback volume set');
      this.ref.tick();
      callback(null);
    }, function(err) {
      console.log('Error: ' + err);
    }, volume);
  }



}
