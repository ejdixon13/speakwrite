import { Injectable } from '@angular/core';
import { AdMobPro } from "@ionic-native/admob-pro";


const ADMOBID = {
    banner: 'ca-app-pub-3817455050750068/9696087192', // 'ca-app-pub-3940256099942544/6300978111', 
    reward: 'ca-app-pub-3940256099942544/5224354917', //'ca-app-pub-3817455050750068/8387997427',
    interstitial: 'ca-app-pub-3817455050750068/8391414544'
};
  
const BANNER_AD_POSITION = 8;// BOTTOM
@Injectable()
export class AdService {

    constructor(private admob: AdMobPro) { }

    prepareBannerAd() {
        this.admob.createBanner({
            adId: ADMOBID.banner,
            isTesting: false,
            autoShow: true,
            position: BANNER_AD_POSITION
        });
     }
    
    showBannerAd() { 
        this.admob.showBanner(BANNER_AD_POSITION);
    }
    hideBannerAd() { 
        this.admob.hideBanner();
    }
    
    prepareVideoAd() { 
        return this.admob.prepareRewardVideoAd({
            adId: ADMOBID.reward,
            isTesting: false,
            autoShow: false
          });
    }
    showVideoAd() {
        if (AdMobPro) {
            this.admob.showRewardVideoAd();
        }
    }

    prepareInterstitialAd() {
        return this.admob.prepareInterstitial({
            adId: ADMOBID.interstitial,
            isTesting: false,
            autoShow: true
        });
    }
    
    // Dont really need this since we auto show
    showInterstitialAd() {
        if (AdMobPro) {
            this.admob.showInterstitial();
        }
    }

}