import { ApplicationRef, Injectable } from "@angular/core";
import { Keyboard } from "@ionic-native/keyboard";
import { some } from "lodash";
import { QuillEditorService } from "../components/quill-editor/quill-editor.service";
import { TextToSpeech } from "../custom-external/text-to-speech";
import { LOCALE, IS_FULLY_SUPPORTED_ANDROID_VERSION } from "../models/app.const";
import { Sentence } from "../models/grammar.model";
import { RecordingService } from "../services/recording.service";
import { WordsService } from "../services/words.service";
import { SettingsService, Settings } from "./settings.service";
import { ToastController } from "ionic-angular";
import { Device } from '@ionic-native/device';


export const PLAYBACK: string = "read back";
export const PLAY_LAST_SENTENCE: string = "read last sentence";
export const PLAY_BACK_SENTENCE: string = "read back sentence";
export const PLAY_LAST_PARAGRAPH: string = "read last paragraph";
export const PLAY_BACK_PARAGRAPH: string = "read back paragraph";
export const PLAY_LAST_WORD: string = "read back word";
export const PLAY_ALL: string = "read all";

export const DELETE_LAST_SENTENCE: string = "delete last sentence";
export const DELETE_LAST_WORD: string = "delete";
export const ERASE_LAST_WORD: string = "erase";
export const ERASE_LAST_SENTENCE: string = "erase last sentence";
export const SCRATCH_THAT: string = "scratch that";

export const NAVIGATE_ONE_WORD_BACK: string = "back";
export const NAVIGATE_TWO_WORDS_BACK: string = "back two words";
const NAVIGATE_THREE_WORDS_BACK: string = "back three words";
const NAVIGATE_FOUR_WORDS_BACK: string = "back four words";
export const NAVIGATE_ONE_WORD_FORWARD: string = "next";
export const NAVIGATE_TWO_WORDS_FORWARD: string = "next two words";
const NAVIGATE_THREE_WORDS_FORWARD: string = "next three words";
const NAVIGATE_FOUR_WORDS_FORWARD: string = "next four words";

export const GO_TO_END: string = "go to end";
export const GO_TO_THE_END: string = "go to the end";

export const OPEN_QUOTE: string = "open quote";
export const CLOSE_QUOTE: string = "close quote";
export const SEMICOLON: string = "semicolon";
export const COLON: string = "colon";

export const CAPITALIZE: string = "capitalize"
export const UNCAPITALIZE: string = "uncapitalize"



enum RecordingState {
  LISTENING,
  NOT_LISTENING
}

@Injectable()
export class CommandService {
  TAG: string = "JARVIS APP";
  recordingState: RecordingState = RecordingState.NOT_LISTENING;
  dictateConfig: { [key: string]: boolean } = {
    isDictateMode: true,
    isWriteMode: false
  };
  settings: Settings

  constructor(
    private tts: TextToSpeech,
    private wordsService: WordsService,
    private quillEditorService: QuillEditorService,
    private recordingService: RecordingService,
    private settingsService: SettingsService,
    private device: Device
  ) {
    this.settingsService.state$.subscribe((settings: Settings) => {
      this.settings = settings
    })
  }

  setPlaybackVolume(volume: number) {
    console.log("volume: " + volume);
    this.recordingService.setPlaybackVolume(volume, () => {
      console.log("volume inside callback: " + volume.toString());
      this.speak(volume.toString());
    });
  }

  startListening() {
    this.recordingState = RecordingState.LISTENING;
    this.recordingService.startListening(matches => {
      this.parseCommand(matches);
    });
  }

  stopListening() {
    this.recordingState = RecordingState.NOT_LISTENING;
    this.recordingService.stopListening(() => {
      this.quillEditorService.scrollToCurrentWord();
    });
  }

  parseCommand(phraseList: Array<string>) {
    const firstPhrase = phraseList[0];

    // may need this for going back on older apis?
    // this.quillEditorService.getCurrentEditor().enable();
    // this.keyboard.close();

    //if no command found, just add text to the document
    if (!some(phraseList, phrase => this.executeCommand(phrase))) {
      this.addWordsAtCursorPosition(firstPhrase);
      if (this.settings.playbackImmediately) {
        this.speak(firstPhrase);
      }
    }
    this.quillEditorService.getCurrentEditor().disable();

  }

  private executeCommand(phrase: string): boolean {
    switch (phrase) {
      case PLAYBACK:
      case PLAY_LAST_SENTENCE:
      case PLAY_BACK_SENTENCE:
        this.playLastSentence();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case PLAY_LAST_PARAGRAPH:
      case PLAY_BACK_PARAGRAPH:
        this.playLastParagraph();
        break;

      case PLAY_LAST_WORD:
        this.playLastWord();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case DELETE_LAST_WORD:
      case ERASE_LAST_WORD:
        this.deleteWordAtCursorPosition();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case DELETE_LAST_SENTENCE:
      case ERASE_LAST_SENTENCE:
        this.deleteLastSentence();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case SCRATCH_THAT:
        this.quillEditorService.deleteLastAddedPhrase();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case NAVIGATE_ONE_WORD_BACK:
      case NAVIGATE_TWO_WORDS_BACK:
      case NAVIGATE_THREE_WORDS_BACK:
      case NAVIGATE_FOUR_WORDS_BACK:
      case NAVIGATE_ONE_WORD_FORWARD:
      case NAVIGATE_TWO_WORDS_FORWARD:
      case NAVIGATE_THREE_WORDS_FORWARD:
      case NAVIGATE_FOUR_WORDS_FORWARD:
        if (IS_FULLY_SUPPORTED_ANDROID_VERSION(this.device.version)) {
          this.navigateCase(phrase);
        } else {
          return false
        }
        break;

      case GO_TO_END:
      case GO_TO_THE_END:
        this.quillEditorService.setCursorToEnd();
        this.quillEditorService.scrollToCurrentWord();
        break;

      case PLAY_ALL:
        this.playbackAll();
        break;

      case OPEN_QUOTE:
        this.addWordsAtCursorPosition('"', true);
        break;
      case CLOSE_QUOTE:
        this.addWordsAtCursorPosition('"');
        break;
      case SEMICOLON:
        this.addWordsAtCursorPosition(';');
        break;
      case COLON:
        this.addWordsAtCursorPosition(':');
        break;
      case UNCAPITALIZE:
        this.quillEditorService.uncapitalizeWordAtCursorPosition();
        break;
      case CAPITALIZE:
        this.quillEditorService.capitalizeWordAtCursorPosition();
        break;
      default:
        return false;
    }
    return true;
  }

  private navigateCase(navigatePhrase: string) {
    switch (navigatePhrase) {
      case NAVIGATE_ONE_WORD_BACK:
        this.quillEditorService.goBackNumWordPuncts(1);
        break;
      case NAVIGATE_TWO_WORDS_BACK:
        this.quillEditorService.goBackNumWordPuncts(2);
        break;
      case NAVIGATE_THREE_WORDS_BACK:
        this.quillEditorService.goBackNumWordPuncts(3);
        break;
      case NAVIGATE_FOUR_WORDS_BACK:
        this.quillEditorService.goBackNumWordPuncts(4);
        break;
      case NAVIGATE_ONE_WORD_FORWARD:
        this.quillEditorService.goForwardNumWordPuncts(1);
        break;
      case NAVIGATE_TWO_WORDS_FORWARD:
        this.quillEditorService.goForwardNumWordPuncts(2);
        break;
      case NAVIGATE_THREE_WORDS_FORWARD:
        this.quillEditorService.goForwardNumWordPuncts(3);
        break;
      case NAVIGATE_FOUR_WORDS_FORWARD:
        this.quillEditorService.goForwardNumWordPuncts(4);
        break;
    }
    this.quillEditorService.updateHighlightedWord();
    this.quillEditorService.scrollToCurrentWord();
  }

  /********************** COMMANDS **************************************/

  private playLastSentence() {
    this.speak(this.quillEditorService.getSentenceAtPosition().value);
  }

  private playLastParagraph() {
    let setences: Sentence[] = this.quillEditorService.getParagraphBeforePosition().sentences;
    this.speakSentences(setences, 50);
  }

  playLastWord() {
    // this.speak(this.codemirrorEditorDocService.getWordAtOrBeforeCursorPosition().value);
  }

  /**
   * Adds pauses between sentences
   * @param sentences
   */
  speakSentences(sentences: Sentence[], pauseTime: number) {
    const recordingState = this.recordingState;

    return this.recordingService.stopListening(() => {
      (function speakSentence() {
        this.tts
          .speak({ text: sentences.splice(0, 1)[0].value, locale: LOCALE, rate: this.settings.TTSPlaybackSpeed, pitch: this.settings.TTSPlaybackPitch })
          .then(() => {
            setTimeout(() => {
              if (sentences.length > 0) {
                speakSentence.call(this);
              } else {
                if (recordingState === RecordingState.LISTENING) {
                  this.startListening();
                }
              }
            }, pauseTime);
          })
          .catch((reason: any) => console.log(reason));
      }.call(this));
    });
  }

  speak(string: string) {
    const recordingState = this.recordingState;
    return this.recordingService.stopListening(() => {
      this.tts
        .speak({ text: string, locale: LOCALE, rate: this.settings.TTSPlaybackSpeed, pitch: this.settings.TTSPlaybackPitch })
        .then(() => {
          if (recordingState === RecordingState.LISTENING) {
            this.startListening();
          }
        })
        .catch((reason: any) => console.log(reason));
    });
  }

  private deleteWordAtCursorPosition() {
    this.quillEditorService.deleteWordAtCusorPosition();
  }

  private deleteLastSentence() {
    this.quillEditorService.deleteSentenceBeforePosition();
    this.recordingService.stopListening(() => {
      this.tts
        .speak({ text: "deleted", locale: LOCALE, rate: this.settings.TTSPlaybackSpeed, pitch: this.settings.TTSPlaybackPitch })
        .then(() => this.startListening())
        .catch((reason: any) => console.log(reason));
    });
  }

  private playbackAll() {
    this.recordingService.stopListening(() => {
      this.tts
        .speak({ text: this.wordsService.getWords().value, locale: LOCALE, rate: this.settings.TTSPlaybackSpeed, pitch: this.settings.TTSPlaybackPitch })
        .then(() => this.startListening())
        .catch((reason: any) => console.log(reason));
    });
  }

  private addWordsAtCursorPosition(words: string, isOpenQuote: boolean = false) {
    this.quillEditorService.addWordsAtCursorPosition(words, isOpenQuote);
  }
}
