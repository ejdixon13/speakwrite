import {Component, ElementRef, ViewChild} from "@angular/core";
import {ViewController} from "ionic-angular";

@Component({
  template: `
    <ion-list #decorate radio-group [(ngModel)]="fontFamily">
      <ion-row>
        <ion-col>
          <button data-command="fontSize|1" ion-item detail-none class="button-command popover-text-button popover-text-smaller">A</button>
        </ion-col>
        <ion-col>
          <button data-command="removeFormat" ion-item detail-none class="button-command popover-text-button">A</button>
        </ion-col>
        <ion-col>
          <button data-command="fontSize|6" ion-item detail-none class="button-command popover-text-button popover-text-larger">A</button>
        </ion-col>
      </ion-row>

      <ion-row>
        <ion-col>
          <button data-command="bold" ion-item detail-none class="button-command popover-text-button"><strong>B</strong></button>
        </ion-col>
        <ion-col>
          <button data-command="italic" ion-item detail-none class="button-command popover-text-button"><i>I</i></button>
        </ion-col>
      </ion-row>
      <ion-item class="popover-text-athelas">
        <ion-label>Athelas</ion-label>
        <ion-radio value="Athelas"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-charter">
        <ion-label>Charter</ion-label>
        <ion-radio value="Charter"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-iowan">
        <ion-label>Iowan</ion-label>
        <ion-radio value="Iowan"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-palatino">
        <ion-label>Palatino</ion-label>
        <ion-radio value="Palatino"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-san-francisco">
        <ion-label>San Francisco</ion-label>
        <ion-radio value="San Francisco"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-seravek">
        <ion-label>Seravek</ion-label>
        <ion-radio value="Seravek"></ion-radio>
      </ion-item>
      <ion-item class="popover-text-times-new-roman">
        <ion-label>Times New Roman</ion-label>
        <ion-radio value="Times New Roman"></ion-radio>
      </ion-item>
    </ion-list>
  `
})
export class RichTextFormattingPage {

  @ViewChild('decorate') decorate: ElementRef;

  constructor(public viewCtrl: ViewController) {}

  close() {
    this.viewCtrl.dismiss();
  }

  private wireupButtons() {
    let buttons = (this.decorate.nativeElement as HTMLDivElement).getElementsByClassName('button-command');
    for (let i = 0; i < buttons.length; i++) {
      let button = buttons[i];

      let command = button.getAttribute('data-command');

      if (command.includes('|')) {
        let parameter = command.split('|')[1];
        command = command.split('|')[0];

        button.addEventListener('click', () => {
          document.execCommand(command, false, parameter);
        });
      } else {
        button.addEventListener('click', () => {
          document.execCommand(command);
        });
      }
    }

  }

  ngAfterContentInit() {
    this.wireupButtons();
  }
}
