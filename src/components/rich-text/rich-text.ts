import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { FormControl } from "@angular/forms";
import {RichTextElementService} from "../../components/rich-text/rich-text-element.service";
import {Keyboard, PopoverController} from "ionic-angular";
import {RichTextFormattingPage} from "./rich-text-formatting";


@Component({
  selector: 'rich-text',
  templateUrl: 'rich-text.html'
})
export class RichTextComponent {


  constructor(private richTextElementService: RichTextElementService, private popoverCtrl: PopoverController, private keyboard: Keyboard) {

  }

  @ViewChild('editor') editor: ElementRef;
  // @ViewChild('decorate') decorate: ElementRef;

  @Input() formControlItem: FormControl;


  private wireupResize() {

    let element = this.editor.nativeElement as HTMLDivElement;

    // let height = (window.innerHeight || document.body.clientHeight) - 250;
    // let textareaHeight = Math.round((height / 100.00) * 45);
    // element.style.height = `${textareaHeight}px`;


  }



  // goToCursor(event) {
  //   event;
  //   this.editor.nativeElement.blur();
  //   this.editor.nativeElement.focus();
  // }

  presentPopover(myEvent) {
    // document.execCommand("InsertHTML", false, "<span class=\"blinking-cursor\">|</span>")
    let popover = this.popoverCtrl.create(RichTextFormattingPage);
    popover.present({
      ev: myEvent
    });
  }

  private init() {
    this.richTextElementService.init(this.editor, this.formControlItem);
  }

  ngAfterContentInit() {

    this.wireupResize();
    // this.updateItem();
    this.init();
    // this.wireupButtons();
  }

}
