import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RichTextComponent } from './rich-text';
import { RichTextFormattingPage } from "./rich-text-formatting";


@NgModule({
  declarations: [
    RichTextComponent,
    RichTextFormattingPage
  ],
  entryComponents: [
    RichTextFormattingPage
  ],
  imports: [
    IonicPageModule.forChild(RichTextComponent),
  ],
  exports: [
    RichTextComponent
  ]
})
export class RichTextComponentModule {}
