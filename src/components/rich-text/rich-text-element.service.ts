import {Injectable, ElementRef} from '@angular/core';
import {FormControl} from "@angular/forms";
import {DocumentService} from "../../services/document.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/debounce';


@Injectable()
export class RichTextElementService {

  editor: HTMLDivElement;

  formControlItem: FormControl;

  priorWords: String;

  cursorPosition: number = 0;

  restoreCursor: Function;

  documentId: any;

  inputStream: Observable<any>;

  constructor(private documentService: DocumentService) {}

  init(editor:ElementRef, formControlItem: FormControl) {
    this.editor = editor.nativeElement as HTMLDivElement;
    this.inputStream = Observable.fromEvent(this.editor, 'input');
    this.formControlItem = formControlItem;
    this.updateElement();
    this.setListenersOnElement();
  }

  updateElement() {
    this.editor.innerHTML = this.formControlItem.value;

    if (this.editor.innerHTML === null || this.editor.innerHTML === '') {
      this.editor.innerHTML = '<div></div>';
      this.formControlItem.setValue('<div></div>');
    }

    if(this.restoreCursor) {
      let offset = this.formControlItem.value.length - this.priorWords.length;
      this.restoreCursor(offset);

      this.cursorPosition += offset;
      this.restoreCursor = this.saveCaretPosition(this.editor);
    }

    this.saveToDevice(this.formControlItem.value);
    this.priorWords = this.formControlItem.value;
  }

  private saveToDevice(body: string) {
    if(this.documentId) {
      // this.documentService.updateDocument(this.documentId, body);
    } else {
      console.error("RichTextElementService: No Document Id to save with");
    }
  }

  getCursorPosition() {
    return this.cursorPosition;
  }

  bindDocument(id) {
    this.documentId = id;
    this.restoreCursor = null;
  }

  private saveCaretPosition(context){
    var selection = window.getSelection();
    var range = selection.getRangeAt(0).cloneRange();
    range.setStart(  context, 0 );
    var len = range.toString().length;

    return function restore(offset:number = 0){
      var deletionOccurred = offset < 0;
      var offsetLen = deletionOccurred ? len + offset : len; // because node already has changes the len needs to be modified by the offset
      var pos = this.getTextNodeAtPosition(context, offsetLen);
      selection.removeAllRanges();
      var range = new Range();
      range.setStart(pos.node , deletionOccurred ? pos.position : pos.position + offset);
      selection.addRange(range);

    }
  }

  private getTextNodeAtPosition(root, index){
    var lastNode = null;

    var treeWalker = document.createTreeWalker(root,
      NodeFilter.SHOW_TEXT,
      {
        acceptNode: (elem)=> {
          if(index > elem.textContent.length){
            index -= elem.textContent.length;
            lastNode = elem;
            return NodeFilter.FILTER_REJECT
          }
          return NodeFilter.FILTER_ACCEPT;
        }
      });
    var c = treeWalker.nextNode();
    return {
      node: c? c: root,
      position: c? index:  0
    };
  }

  // get hasFocusedInput() {
  //   return this.keyboard.hasFocusedTextInput();
  // }

  private setListenersOnElement(){
    let updateItem = () => {
      this.formControlItem.setValue(this.editor.innerHTML);
      let range = window.getSelection().getRangeAt(0);
      this.cursorPosition = this.getCharacterOffsetWithin(range, this.editor);
      this.restoreCursor = this.saveCaretPosition(this.editor);
      this.priorWords = this.formControlItem.value;
    };

    this.editor.onchange = () => updateItem();
    this.editor.onkeyup = () => updateItem();
    this.editor.onpaste = () => updateItem();
    this.editor.oninput = () => updateItem();
    this.editor.onclick = () => updateItem();

    //used for physical keyboard input saves
    this.inputStream
      .debounceTime(2000)
      .subscribe(() =>{
        this.updateElement();
      })
  }

  private getCharacterOffsetWithin(range, node) {
    var treeWalker = document.createTreeWalker(
      node,
      NodeFilter.SHOW_TEXT,
      {
        acceptNode: (node) => {
          var nodeRange = document.createRange();
          nodeRange.selectNode(node);
          return nodeRange.compareBoundaryPoints(Range.END_TO_END, range) < 1 ?
            NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT;
        }
      },
      false
    );

    var charCount = 0;
    while (treeWalker.nextNode()) {
      charCount += treeWalker.currentNode.nodeValue.length;
    }

    if (range.startContainer.nodeType == Node.TEXT_NODE) {
      charCount += range.startOffset;
    }
    return charCount;
  }



}
