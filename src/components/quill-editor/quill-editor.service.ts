import { ApplicationRef, Injectable } from "@angular/core";
import { includes, words, lowerFirst, upperFirst } from "lodash";
import * as QuillNamespace from "quill";
import { RangeStatic } from "quill";
import { Subject } from "rxjs";
import { IGNORE_ALL_PUNCTUATION_REGEX, IGNORE_PUNCTUATION_REGEX, MATCH_ALL_PUNCTUATION_REGEX, Paragraph, PUNCTUATION, Sentence, VALID_SENTENCE_END, WordPunct } from "../../models/grammar.model";
import { PaidVsFreeService } from '../../services/paidVsFree.service';
import { getWordPunctAtIndex, getPriorWordPunct, getNextWordPunct, getSentenceAtIndex } from '../../util/word-punct.util';
const TAG: string = "JARVIS APP";
let Quill: any = QuillNamespace;

const LONGEST_WORD_LENGTH = 20;

@Injectable()
export class QuillEditorService {
  currentEditor: QuillNamespace.Quill;
  lastAddedPhrase: Sentence;
  lastAddedPhraseIsOpenQuote: boolean = false;
  private editorScrollSource = new Subject<any>();
  editorScrollMethodCalled$ = this.editorScrollSource.asObservable();

  constructor(private paidVsFreeService: PaidVsFreeService) { }
  

  updateHighlightedWord() {
    let quillEditor = this.getCurrentEditor();

    // let wordAtCursorPos: Word = this.getWordAtCursorPositionIgnorePunctuation();
    let wordAtCursorPos: WordPunct = this.getWordPunctAtPosition();
    quillEditor.removeFormat(0, quillEditor.getLength() - 1);
    quillEditor.formatText(wordAtCursorPos.position.index, wordAtCursorPos.position.length, "background", "pink");

    // TODO: remove temp code
    const selection = this.getSelection(false);
    quillEditor.formatText(selection.index, selection.length, "background", "blue");

  }

  setCursorToEnd() {
    this.currentEditor.setSelection(this.currentEditor.getLength(), 0);
    const lastWord = this.getWordPunctAtPosition();
    if (lastWord) {
      this.setWordPunctSelection(lastWord);
    }
    this.updateHighlightedWord();
  }

  addWordsAtCursorPosition(words: string, isOpenQuote: boolean = false) {
    let formattedWords = words;

    // add words to count if free
    if (!this.paidVsFreeService.isPaid()) {
      this.paidVsFreeService.addToWordsUsed(words.split(' ').length);
    }

    //check for sentence end
    let word = this.getWordPunctAtPosition();
    console.log("Word at Position: " + word.value);
    if (includes([...VALID_SENTENCE_END, '"'], word.value) || this.isBeginningOfDocument(word.position.index)) {
      formattedWords = formattedWords.charAt(0).toUpperCase() + formattedWords.slice(1);
    }

    // check for punctuation, to add space
    if (!this.lastAddedPhraseIsOpenQuote) {
      formattedWords = includes([...VALID_SENTENCE_END, ...PUNCTUATION], formattedWords[0])
        ? formattedWords
        : " " + formattedWords;
    }

    const index1 = this.getSelection(false).index;

    // save off last added phrase
    this.lastAddedPhrase = new Sentence(formattedWords, {
      index: index1,
      length: formattedWords.length
    });
    this.lastAddedPhraseIsOpenQuote = isOpenQuote;

    if (this.isNewLineAtCursor()) {
      this.currentEditor.insertText(this.getSelection(false).index, formattedWords);
    } else {
      const wordAtPosition = this.getWordPunctAtPosition(null, false);
      const index2 = wordAtPosition.position.index + wordAtPosition.position.length;
      this.currentEditor.insertText(index2, formattedWords);
      this.currentEditor.setSelection(index2 + formattedWords.length, 0);
    }
    this.scrollToCurrentWord(true);
  }

  private isNewLineAtCursor() {
    const cursor = this.getSelection(false);
    const content = this.currentEditor.getText(cursor.index - 1, 1); 
    return content === '\n';
  }

  private isBeginningOfDocument(index: number) {
    while (!this.currentEditor.getText(index, 1).trim() && index > 0) {
      --index;
    }
    return index <= 0;
  }

  scrollToCurrentWord(smooth: boolean = false) {
    setTimeout(() => {
      const highlightedWord = document.querySelector('span[style*="background-color"]');
      if (highlightedWord) {
        highlightedWord
          .scrollIntoView({
            block: 'center',
            behavior: smooth ? 'smooth' : 'auto'
          });
      }
    }, 100)
  }

  getCurrentEditor(): QuillNamespace.Quill {
    return this.currentEditor;
  }

  setCurrentEditor(editor: QuillNamespace.Quill) {
    this.currentEditor = editor;
  }

  getWordPunctAtPosition(position?: RangeStatic, focus:boolean = true): WordPunct {
    if (!position) {
      position = this.getSelection(focus);
    }
    return getWordPunctAtIndex(position.index, this.currentEditor.getText());
  }

  private getSelection(focus: boolean = true): RangeStatic {
    let position: RangeStatic = this.currentEditor.getSelection(focus);

    // try https://stackoverflow.com/questions/39359444/quill-js-unable-to-get-selection-range-on-a-readonlytrue-editor-in-quill-js-0
    if (!position) {
      position = (this.currentEditor as any).selection.savedRange;
    }
    console.log('Position Index: ' + position.index);
    console.log('Position Length: ' + position.length);

    return position;
  }

  goBackNumWordPuncts(numWords: number = 1) {
    const currentWordPunct = this.getWordPunctAtPosition();
    const docText = this.currentEditor.getText();
    let priorWordPunct = getPriorWordPunct(currentWordPunct.endIndex, docText);
    numWords--;

    while (numWords > 0) {
      priorWordPunct = getPriorWordPunct(priorWordPunct.endIndex, docText);
      numWords--;
    }
    this.setWordPunctSelection(priorWordPunct);
  }

  setWordPunctSelection(wordPunct: WordPunct) {
    this.currentEditor.setSelection(wordPunct.position.index + wordPunct.position.length, 0);
  }

  goForwardNumWordPuncts(numWords: number) {
    const currentWordPunct = this.getWordPunctAtPosition();
    const docText = this.currentEditor.getText();
    let nextWordPunct = getNextWordPunct(currentWordPunct.endIndex, docText);
    numWords--;

    while (numWords > 0) {
      nextWordPunct = getNextWordPunct(nextWordPunct.endIndex, docText);
      numWords--;
    }
    this.setWordPunctSelection(nextWordPunct);
  }

  getSentenceAtPosition(position: RangeStatic = this.getSelection()): Sentence {
    return getSentenceAtIndex(position.index, this.currentEditor.getText());
  }


  getParagraphBeforePosition(): Paragraph {
    let sentences: Sentence[] = [];
    // get first sentence
    const lastSentenceOfParagraph = this.getSentenceAtPosition();
    sentences.push(lastSentenceOfParagraph);
    // set paragraph end
    // get sentence range begin to input to while loop
    let rangePtr: RangeStatic = lastSentenceOfParagraph.range;

    //while not paragraph beginning
    while (!this.isParagraphBeginning(rangePtr.index)) {
      sentences.unshift(this.getSentenceAtPosition(rangePtr));
      rangePtr = sentences[0].range;
    }

    let index = sentences[0].range.index;
    let length = lastSentenceOfParagraph.range.index + lastSentenceOfParagraph.range.length - index;
    return new Paragraph(sentences, { index: index, length: length });
  }

  //more than 3 spaces, or new line and not beginning of doc
  private isParagraphBeginning(index: number) {
    if (index < 3) {
      return true;
    }
    let spaceCheckStr = this.currentEditor.getText(index, 3);

    let containsTabOrNewLine = /[\t\r\n↵]/.test(spaceCheckStr);
    let isThreeSpaces = /^\s\s\s$/.test(spaceCheckStr);
    return containsTabOrNewLine || isThreeSpaces;
  }
  
  uncapitalizeWordAtCursorPosition() {
    const wordToUncapitalize = this.getWordPunctAtPosition();

    this.currentEditor.deleteText(wordToUncapitalize.position.index, wordToUncapitalize.position.length);
    this.currentEditor.insertText(wordToUncapitalize.position.index, lowerFirst(wordToUncapitalize.value));
  }
  capitalizeWordAtCursorPosition() {
    const wordToCapitalize = this.getWordPunctAtPosition();

    this.currentEditor.deleteText(wordToCapitalize.position.index, wordToCapitalize.position.length);
    this.currentEditor.insertText(wordToCapitalize.position.index, upperFirst(wordToCapitalize.value));
  }

  deleteWordAtCusorPosition() {
    let word = this.getWordPunctAtPosition();
    let spaceAdjustedIndex = word.position.index; // delete spaces preceding the word as well
    let lengthOffset = 0;
    while (!this.currentEditor.getText(spaceAdjustedIndex - 1, 1).trim() && spaceAdjustedIndex - 1 > 0) {
      spaceAdjustedIndex--;
      lengthOffset++;
    }

    this.currentEditor.deleteText(spaceAdjustedIndex, word.position.length + lengthOffset);
    let nextWord = this.getWordPunctAtPosition();
    this.setWordPunctSelection(nextWord);
  }

  deleteSentenceBeforePosition(position: RangeStatic = this.getSelection()) {
    const sentence: Sentence = this.getSentenceAtPosition(position);
    let endIndex = sentence.range.index + sentence.range.length;
    while (includes(VALID_SENTENCE_END, this.currentEditor.getText(++endIndex, 1))) {}
    this.currentEditor.deleteText(sentence.range.index, endIndex - sentence.range.index);
  }

  deleteRange(range: RangeStatic) {
    this.currentEditor.deleteText(range.index, range.length);
  }

  deleteLastAddedPhrase() {
    if (this.lastAddedPhrase) {
      this.deleteRange(this.lastAddedPhrase.range);
      this.lastAddedPhrase = null;
    }
  }

  getPassiveVoice(script) {
    /**
     var fs = this;
     if (script.text.length > maxScanLen) {
        return[];
      }
     var contextElem = script.text.match(/\s(is|are|was|were|be|been|being)\s([a-z]{2,30})\b(\sby\b)?/gi);
     return contextElem ? contextElem.filter(function(segment) {
        var parts = segment.match(/([a-z]+)\b(\sby\b)?$/i);
        if (!parts) {
          return false;
        }
        var part = parts[1];
        var getPassiveVoices = part.match(/ed$/) || void 0 !== out.default[part.toLowerCase()];
        return getPassiveVoices;
      }).map(function(requestUrl) {
        return requestUrl.replace(/^\s/, "");
      }).map(function(lines, callback, details) {
        return fs.buildTreeFragment(self.default.tokens.passiveVoice, lines, fs.getPassiveIndex(script, lines, callback, details));
      }).map(function(opt) {
        return fs.addTrailingSpaceSetting(opt, script);
      }) : [];
     **/
  }

  getAdverbs() {
    /**
     var dojo = this;
     return s.items.filter(function(script) {
        return script.text.match(/ly$/);
      }).filter(function(target) {
        return void 0 === obj.default[target.text.toLowerCase()];
      }).map(function(node) {
        return dojo.buildTreeFragment(self.default.tokens.adverb, node.text, node.startIndex);
      }).map(function(tag) {
        return dojo.addTrailingSpaceSetting(tag, s);
      });
     **/
  }

  getItemDelimiter(dataAndEvents) {
    switch (dataAndEvents) {
      case "root":
        return "[\n]+";
      case "paragraph":
        return "[.?!]{1,2}[\"\u201d'\\)]?(?:\\s|$)";
      case "sentence":
        return "[^\\w'-]";
      default:
        return "";
    }
  }

  getSubType(deepDataAndEvents) {
    switch (deepDataAndEvents) {
      case "root":
        return "paragraph";
      case "paragraph":
        return "sentence";
      case "sentence":
        return "word";
      default:
        return "";
    }
  }
}
